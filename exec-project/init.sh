#!/bin/sh
while :
do
  read -p "Options: 1- Install 2- Start Producer 3- Start Consumer: " INPUT_STRING
  case $INPUT_STRING in
	  1)
      echo "Step 1: Creating containers oracle-xe-11g, apache-zookeeper, apache-kafka." && \
      docker-compose up -d && \

      echo "Step 2: Kafka Topics." && \
      echo "Waiting for 15 seconds..." && \
      sleep 15 && \

      echo "Creating KAF40021-1-0-SupplyChainErpService-InventoryAdjusment Topic..." && \
      docker exec -it apache-kafka bash -c "/opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic KAF40021-1-0-SupplyChainErpService-InventoryAdjusment" && \

      echo "Creating KAF40022-1-0-SupplyChainErpService-OperationError Topic..." && \
      docker exec -it apache-kafka bash -c "/opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic KAF40022-1-0-SupplyChainErpService-OperationError" && \

      echo "Creating KAF40023-1-0-SupplyChainErpService-OperationSuccess Topic..." && \
      docker exec -it apache-kafka bash -c "/opt/bitnami/kafka/bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic KAF40023-1-0-SupplyChainErpService-OperationSuccess" && \
      exit
		  ;;
	  2)
        echo "KAF40021-1-0-SupplyChainErpService-InventoryAdjusment Producer..." && \
        docker exec -i apache-kafka bash -c "/opt/bitnami/kafka/bin/kafka-console-producer.sh --bootstrap-server localhost:9092 --topic KAF40021-1-0-SupplyChainErpService-InventoryAdjusment"
		    ;;
    3)
		while :
        do
            read -p "Consumer: 1- OperationError 2- OperationSuccess: " INPUT_STRING_2
            case $INPUT_STRING_2 in
            1)
              echo "KAF40022-1-0-SupplyChainErpService-OperationError Consumer..." && \
              docker exec -i apache-kafka bash -c "/opt/bitnami/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic KAF40022-1-0-SupplyChainErpService-OperationError --from-beginning"
              ;;
            2)
              echo "KAF40023-1-0-SupplyChainErpService-OperationSuccess Consumer..." && \
              docker exec -i apache-kafka bash -c "/opt/bitnami/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic KAF40023-1-0-SupplyChainErpService-OperationSuccess --from-beginning"
              ;;
            *)
                echo "Invalid option!"
                exit
                ;;
            esac
        done
		;;
	*)
		echo "Invalid option!"
    exit
		;;
  esac
done