package com.ws.wom.cl.constants;

/**
 * 
 * @author Reinaldo Porto
 *
 */
public interface IConstants {
	/**
	 * bootstrap servers configuration constant
	 */
	public static final String BOOTSTRAP_SERVERS_CONFIG = "localhost:9092";
	/**
	 * group id constant
	 */
	public static final String GROUP_ID_CONFIG = "inventory_adjustment_group";
	/**
	 * inventory adjustment topic constant
	 */
	public static final String INVENTORY_ADJUSTMENT_TOPIC = "KAF40021-1-0-SupplyChainErpService-InventoryAdjusment";
	/**
	 * operation success topic
	 */
	public static final String OPERATION_SUCCESS_TOPIC = "KAF40023-1-0-SupplyChainErpService-OperationSuccess";
	/**
	 * operation error topic
	 */
	public static final String OPERATION_ERROR_TOPIC = "KAF40022-1-0-SupplyChainErpService-OperationError";
	/**
	 * concurrent kafka listener constant
	 */
	public static final String CONCURRENT_KAFKA_LISTENER = "concurrentKafkaListenerContainerFactory";
}
