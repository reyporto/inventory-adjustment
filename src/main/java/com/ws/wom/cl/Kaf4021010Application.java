package com.ws.wom.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Kaf4021010Application {

	public static void main(String[] args) {
		SpringApplication.run(Kaf4021010Application.class, args);
	}

}
