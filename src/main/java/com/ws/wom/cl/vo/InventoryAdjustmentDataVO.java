package com.ws.wom.cl.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Kafka Producer KAF40021-1-0-SupplyChainErpService-InventoryAdjusment JSON message.
 * 
 * @author Reinaldo Porto
 *
 */
@Data
@NoArgsConstructor
@SuppressWarnings("unused")
public class InventoryAdjustmentDataVO {
	/**
	 * prop unidadOperativa
	 */
	private String unidadOperativa;
	/**
	 * prop codOrganizacionDesde
	 */
	private String codOrganizacionDesde;
	/**
	 * prop codOrganizacionHasta
	 */
	private String codOrganizacionHasta;
	/**
	 * prop transactionType
	 */
	private String transactionType;
	/**
	 * prop codigoSku
	 */
	private String codigoSku;
	/**
	 * prop unidadMedida
	 */
	private String unidadMedida;
	/**
	 * prop cantidad
	 */
	private int cantidad;
	/**
	 * prop canalVenta
	 */
	private String canalVenta;
	/**
	 * prop serie
	 */
	private String serie;
	/**
	 * prop subinventoryCodeDesde
	 */
	private String subinventoryCodeDesde;
	/**
	 * prop subinventoryCodeHasta
	 */
	private String subinventoryCodeHasta;
	/**
	 * prop transactionDate
	 */
	private String transactionDate;
	/**
	 * prop atributte1
	 */
	private String atributte1;
	/**
	 * prop atributte2
	 */
	private String atributte2;
	/**
	 * prop atributte3
	 */
	private String atributte3;
	/**
	 * prop atributte4
	 */
	private String atributte4;
	/**
	 * prop atributte5
	 */
	private String atributte5;
	/**
	 * prop createdBy
	 */
	private int createdBy;
	/**
	 * prop createdDate
	 */
	private String createdDate;
	/**
	 * prop lastUpdateDate
	 */
	private String lastUpdateDate;
	/**
	 * prop lastUpdateBy
	 */
	private int lastUpdateBy;
}
