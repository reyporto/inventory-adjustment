package com.ws.wom.cl.vo;

import java.util.ArrayList;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Kafka Producer KAF40021-1-0-SupplyChainErpService-InventoryAdjusment JSON message.
 * 
 * @author Reinaldo Porto
 *
 */
@Data
@NoArgsConstructor
@SuppressWarnings("unused")
public class InventoryAdjustmentVO {
	/**
	 * prop id
	 */
	private String id;
	/**
	 * prop user
	 */
	private String user;
	/**
	 * prop nombreOperacion
	 */
	private String nombreOperacion;
	/**
	 * prop fecha_mensaje
	 */
	private long fecha_mensaje;
	/**
	 * prop data
	 */
	private ArrayList<InventoryAdjustmentDataVO> data;
}
