package com.ws.wom.cl.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ws.wom.cl.db.IInvWmAdjustments;
import com.ws.wom.cl.db.InvWmAdjustments;

/**
 * App Controller.
 * 
 * @author Reinaldo Porto
 *
 */
@Controller
public class AppController {
	@Autowired
	private IInvWmAdjustments invWmAdjustments;
	
	@RequestMapping("")
	public String index(Model model) {
		ArrayList<InvWmAdjustments> invWmAdjustmentsList = (ArrayList<InvWmAdjustments>) this.invWmAdjustments.findAll();
		model.addAttribute("invWmAdjustmentsList", invWmAdjustmentsList);
		return "index.jsp";
	}
}
