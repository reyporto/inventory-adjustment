package com.ws.wom.cl.db;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IInvWmAdjustments extends JpaRepository<InvWmAdjustments, Integer> { }
