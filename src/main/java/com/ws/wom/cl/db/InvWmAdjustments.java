package com.ws.wom.cl.db;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

/**
 * Entity XXNCL_INV_WMSAJUSTES_INTERFACE
 * 
 * @author Reinaldo Porto
 *
 */
@Entity
@Table(name="XXNCL_INV_WMSAJUSTES_INTERFACE")
@Data
public class InvWmAdjustments {
	/**
	 * column ID
	 */
	@Id
	@Column(name="ID")
	@SequenceGenerator(name="sequence", sequenceName="AJUTSMENT_SEQUENCE", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence")
	private int id;
	/**
	 * column LOAD_ID
	 */
	@Column(name="LOAD_ID")
	private int loadId;
	/**
	 * column FILE_NAME
	 */
	@Column(name="FILE_NAME")
	private String fileName;
	/**
	 * column MENSAJE
	 */
	@Column(name="MENSAJE")
	private String mensaje;
	/**
	 * column NRODOCUMENTO
	 */
	@Column(name="NRODOCUMENTO")
	private int nroDocumento;
	/**
	 * column NROLINEA
	 */
	@Column(name="NROLINEA")
	private int nroLinea;
	/**
	 * column FECHATRANSACCION
	 */
	@Column(name="FECHATRANSACCION")
	private Date fechaTransaccion;
	/**
	 * column CODIGOMATERIAL
	 */
	@Column(name="CODIGOMATERIAL")
	private String codigoMaterial;
	/**
	 * column CANTIDAD
	 */
	@Column(name="CANTIDAD")
	private int cantidad;
	/**
	 * column NUMEROSERIE
	 */
	@Column(name="NUMEROSERIE")
	private String numeroSerie;
	/**
	 * column ORGORIGEN
	 */
	@Column(name="ORGORIGEN")
	private String orgorigen;
	/**
	 * column SUBINVORIGEN
	 */
	@Column(name="SUBINVORIGEN")
	private String subInvOrigen;
	/**
	 * column ORGDESTINO
	 */
	@Column(name="ORGDESTINO")
	private String orgDestino;
	/**
	 * column SUBINVDESTINO
	 */
	@Column(name="SUBINVDESTINO")
	private String subInvDestino;
	/**
	 * column LOCADESTINO
	 */
	@Column(name="LOCADESTINO")
	private String locaDestino;
	/**
	 * column COMENTARIO
	 */
	@Column(name="COMENTARIO")
	private String comentarion;
	/**
	 * column MOTIVO
	 */
	@Column(name="MOTIVO")
	private String motivo;
	/**
	 * column RUTESP
	 */
	@Column(name="RUTESP")
	private String rutEsp;
	/**
	 * column RUTCLIENTE
	 */
	@Column(name="RUTCLIENTE")
	private String rutCliente;
	/**
	 * column RUTPROVEEDOR
	 */
	@Column(name="RUTPROVEEDOR")
	private String rutProveedor;
	/**
	 * column CODDIRECCION
	 */
	@Column(name="CODDIRECCION")
	private String codDireccion;
	/**
	 * column IDREFERENCIA
	 */
	@Column(name="IDREFERENCIA")
	private int idReferencia;
	/**
	 * column LINEACONTROL
	 */
	@Column(name="LINEACONTROL")
	private int lineaControl;
	/**
	 * column TOTALCONTROL
	 */
	@Column(name="TOTALCONTROL")
	private int totalControl;
	/**
	 * column STATUS
	 */
	@Column(name="STATUS")
	private String status;
	/**
	 * column ERROR_EXPLANATION
	 */
	@Column(name="ERROR_EXPLANATION")
	private String errorExplanation;
	/**
	 * column DOCUMENT_HEADER_ID
	 */
	@Column(name="DOCUMENT_HEADER_ID")
	private int documentHeaderId;
	/**
	 * column DOCUMENT_LINE_ID
	 */
	@Column(name="DOCUMENT_LINE_ID")
	private int documentLineId;
	/**
	 * column ITEM_ID
	 */
	@Column(name="ITEM_ID")
	private int itemId;
	/**
	 * column FROM_ORG_ID
	 */
	@Column(name="FROM_ORG_ID")
	private int fromOrgId;
	/**
	 * column TO_ORG_ID
	 */
	@Column(name="TO_ORG_ID")
	private int toOrgId;
	/**
	 * column FROM_LOCATOR_ID
	 */
	@Column(name="FROM_LOCATOR_ID")
	private int fromLocatorId;
	/**
	 * column TO_LOCATOR_ID
	 */
	@Column(name="TO_LOCATOR_ID")
	private int toLocatorId;
	/**
	 * column ORG_ID
	 */
	@Column(name="ORG_ID")
	private int orgId;
	/**
	 * column CODE_COMBINATION_ID
	 */
	@Column(name="CODE_COMBINATION_ID")
	private int codeCombinationId;
	/**
	 * column INV_TRANSACTION_ID
	 */
	@Column(name="INV_TRANSACTION_ID")
	private int invTransactionId;
	/**
	 * column TRANSACTION_TYPE_ID
	 */
	@Column(name="TRANSACTION_TYPE_ID")
	private int transactionTypeId;
	/**
	 * column SEND_DATE
	 */
	@Column(name="SEND_DATE")
	private Date sendDate;
	/**
	 * column CREATED_BY
	 */
	@Column(name="CREATED_BY")
	private int createdBy;
	/**
	 * column CREATION_DATE
	 */
	@Column(name="CREATION_DATE")
	private Date creationDate;
	/**
	 * column LAST_UPDATE_BY
	 */
	@Column(name="LAST_UPDATE_BY")
	private int lastUpdateBy;
	/**
	 * column LAST_UPDATED_DATE
	 */
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdateDate;
	/**
	 * column UNIDADMEDIDA
	 */
	@Column(name="UNIDADMEDIDA")
	private String unidadMedida;
	/**
	 * column FECHADESPACHO
	 */
	@Column(name="FECHADESPACHO")
	private Date fechaDespacho;
	/**
	 * column FECHAPROGRAMADA
	 */
	@Column(name="FECHAPROGRAMADA")
	private Date fechaProgramada;
	/**
	 * column FECHARECEPCION
	 */
	@Column(name="FECHARECEPCION")
	private Date fechaRecepcion;
	/**
	 * column GUIADESPACHO
	 */
	@Column(name="GUIADESPACHO")
	private int guiaDespacho;
	/**
	 * column INTERFACE_HEADER_ID
	 */
	@Column(name="INTERFACE_HEADER_ID")
	private int interfaceHeaderId;
	/**
	 * column INTERFACE_LINE_ID
	 */
	@Column(name="INTERFACE_LINE_ID")
	private int interfaceLineId;
	/**
	 * column SHIPMENT_NUMBER
	 */
	@Column(name="SHIPMENT_NUMBER")
	private int shipmentNumber;
	/**
	 * column SHIPMENT_HEADER_ID
	 */
	@Column(name="SHIPMENT_HEADER_ID")
	private int shipmentHeaderId;
	/**
	 * column SHIPMENT_LINE_ID
	 */
	@Column(name="SHIPMENT_LINE_ID")
	private int shipmentLineId;
	/**
	 * column PO_HEADER_ID
	 */
	@Column(name="PO_HEADER_ID")
	private int poHeaderId;
	/**
	 * column PO_LINE_LOCATION_ID
	 */
	@Column(name="PO_LINE_LOCATION_ID")
	private int poLineLocationId;
	/**
	 * column DESTINATION_TYPE_CODE
	 */
	@Column(name="DESTINATION_TYPE_CODE")
	private String destinationTypeCode;
	/**
	 * column VENDOR_ID
	 */
	@Column(name="VENDOR_ID")
	private int vendorId;
	/**
	 * column VENDOR_SITE_ID
	 */
	@Column(name="VENDOR_SITE_ID")
	private int vendorSiteId;
	/**
	 * column PO_LINE_ID
	 */
	@Column(name="PO_LINE_ID")
	private int poLineId;
	/**
	 * column SHIPMENT_LINE
	 */
	@Column(name="SHIPMENT_LINE")
	private int shipmentLine;
	/**
	 * column LOCATION_ID
	 */
	@Column(name="LOCATION_ID")
	private int locationId;
	/**
	 * column PERSON_ID
	 */
	@Column(name="PERSON_ID")
	private int personId;
	/**
	 * column CANAL_VENTA
	 */
	@Column(name="CANAL_VENTA")
	private String canalVenta;
	/**
	 * column UNIDAD_OPERATIVA
	 */
	@Column(name="UNIDAD_OPERATIVA")
	private String unidadOperativa;
	/**
	 * column TRANSACTION_TYPE_WMS
	 */
	@Column(name="TRANSACTION_TYPE_WMS")
	private String transactionTypeWms;
	/**
	 * column CANTSPROV
	 */
	@Column(name="CANTSPROV")
	private int cantSprov;
	/**
	 * column CANTCPROV
	 */
	@Column(name="CANTCPROV")
	private int cantCprov;
	/**
	 * column ACCRUAL_CCID
	 */
	@Column(name="ACCRUAL_CCID")
	private int accrualCcid;
	/**
	 * column VARIANCE_CCID
	 */
	@Column(name="VARIANCE_CCID")
	private int varianceCcid;
	/**
	 * column BUDGET_CCID
	 */
	@Column(name="BUDGET_CCID")
	private int budgetCcid;
	/**
	 * column TIPO_CIERRE_OC
	 */
	@Column(name="TIPO_CIERRE_OC")
	private char tipoCierreOc;
	/**
	 * column ESTADO_RECEPCION_ON
	 */
	@Column(name="ESTADO_RECEPCION_ON")
	private int estadoRecepcionOn;
	/**
	 * column ATRIBUTTE1
	 */
	@Column(name="ATRIBUTTE1")
	private String atributte1;
	/**
	 * column ATRIBUTTE2
	 */
	@Column(name="ATRIBUTTE2")
	private String atributte2;
	/**
	 * column ATRIBUTTE3
	 */
	@Column(name="ATRIBUTTE3")
	private String atributte3;
	/**
	 * column ATRIBUTTE4
	 */
	@Column(name="ATRIBUTTE4")
	private String atributte4;
	/**
	 * column ATRIBUTTE5
	 */
	@Column(name="ATRIBUTTE5")
	private String atributte5;
}
