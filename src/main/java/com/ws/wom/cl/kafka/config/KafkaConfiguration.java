package com.ws.wom.cl.kafka.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.ws.wom.cl.constants.IConstants;
import com.ws.wom.cl.vo.InventoryAdjustmentVO;

/**
 * Kafka Configuration Consumer and Listener Factory.
 * 
 * @author Reinaldo Porto
 *
 */
@EnableKafka
@Configuration
public class KafkaConfiguration {
	
	@Bean
	public ConsumerFactory<String, InventoryAdjustmentVO> consumerFactory() {
		Map<String, Object> configs = new HashMap<>();
		configs.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, IConstants.BOOTSTRAP_SERVERS_CONFIG);
		configs.put(ConsumerConfig.GROUP_ID_CONFIG, IConstants.GROUP_ID_CONFIG);
		configs.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		configs.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		
		return new DefaultKafkaConsumerFactory<>(configs, new StringDeserializer(), 
				new ErrorHandlingDeserializer<>(new JsonDeserializer<>(InventoryAdjustmentVO.class)));
	}
	
	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, InventoryAdjustmentVO> concurrentKafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, InventoryAdjustmentVO> containerFactory = new ConcurrentKafkaListenerContainerFactory<>();
		containerFactory.setErrorHandler(new SeekToCurrentErrorHandler());
		containerFactory.setConsumerFactory(this.consumerFactory());
		
		return containerFactory;
	}
}
