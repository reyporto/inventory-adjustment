package com.ws.wom.cl.kafka.listener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.ws.wom.cl.constants.IConstants;
import com.ws.wom.cl.db.IInvWmAdjustments;
import com.ws.wom.cl.db.InvWmAdjustments;
import com.ws.wom.cl.vo.InventoryAdjustmentDataVO;
import com.ws.wom.cl.vo.InventoryAdjustmentVO;

/**
 * Kafka Consumer.
 * 
 * @author Reinaldo Porto
 *
 */
@Service
public class KafkaConsumer {
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	@Autowired
	private IInvWmAdjustments invWmAdjustments;
	
	@KafkaListener(topics = IConstants.INVENTORY_ADJUSTMENT_TOPIC, groupId = IConstants.GROUP_ID_CONFIG, 
			containerFactory = IConstants.CONCURRENT_KAFKA_LISTENER)
	public void inventoryAdjustment(InventoryAdjustmentVO message) {
		System.out.println("Id :: " + message.getId());
		System.out.println("Usuario :: " + message.getUser());
		System.out.println("Nombre Operación :: " + message.getNombreOperacion());
		System.out.println("Fecha Mensaje :: " + this.getMessageDate(message.getFecha_mensaje()));
		
		ArrayList<InvWmAdjustments> invWmAdjustmentsList = new ArrayList<>();
		InvWmAdjustments invWmAdjustmentsObj;
		
		try {
			for (InventoryAdjustmentDataVO data : message.getData()) {
				invWmAdjustmentsObj = new InvWmAdjustments();
				
				invWmAdjustmentsObj.setLoadId(0);
				invWmAdjustmentsObj.setFileName("");
				invWmAdjustmentsObj.setMensaje("");
				invWmAdjustmentsObj.setNroDocumento(0);
				invWmAdjustmentsObj.setNroLinea(0);
				invWmAdjustmentsObj.setFechaTransaccion(this.stringToSqlDate(data.getTransactionDate()));
				invWmAdjustmentsObj.setCodigoMaterial(data.getCodigoSku());
				invWmAdjustmentsObj.setCantidad(data.getCantidad());
				invWmAdjustmentsObj.setNumeroSerie(data.getSerie());
				invWmAdjustmentsObj.setOrgorigen(data.getCodOrganizacionDesde());
				invWmAdjustmentsObj.setSubInvOrigen(data.getSubinventoryCodeDesde());
				invWmAdjustmentsObj.setOrgDestino(data.getCodOrganizacionHasta());
				invWmAdjustmentsObj.setSubInvDestino(data.getSubinventoryCodeHasta());
				invWmAdjustmentsObj.setLocaDestino("");
				invWmAdjustmentsObj.setComentarion("");
				invWmAdjustmentsObj.setMotivo("");
				invWmAdjustmentsObj.setRutEsp("");
				invWmAdjustmentsObj.setRutCliente("");
				invWmAdjustmentsObj.setRutProveedor("");
				invWmAdjustmentsObj.setCodDireccion("");
				invWmAdjustmentsObj.setIdReferencia(0);
				invWmAdjustmentsObj.setLineaControl(0);
				invWmAdjustmentsObj.setTotalControl(0);
				invWmAdjustmentsObj.setStatus("");
				invWmAdjustmentsObj.setErrorExplanation("");
				invWmAdjustmentsObj.setDocumentHeaderId(0);
				invWmAdjustmentsObj.setDocumentLineId(0);
				invWmAdjustmentsObj.setItemId(0);
				invWmAdjustmentsObj.setFromOrgId(0);
				invWmAdjustmentsObj.setToOrgId(0);
				invWmAdjustmentsObj.setFromLocatorId(0);
				invWmAdjustmentsObj.setToLocatorId(0);
				invWmAdjustmentsObj.setOrgId(0);
				invWmAdjustmentsObj.setCodeCombinationId(0);
				invWmAdjustmentsObj.setInvTransactionId(0);
				invWmAdjustmentsObj.setTransactionTypeId(0);
				invWmAdjustmentsObj.setSendDate(null);
				invWmAdjustmentsObj.setCreatedBy(data.getCreatedBy());
				invWmAdjustmentsObj.setCreationDate(this.stringToSqlDate(data.getCreatedDate()));
				invWmAdjustmentsObj.setLastUpdateBy(data.getLastUpdateBy());
				invWmAdjustmentsObj.setLastUpdateDate(this.stringToSqlDate(data.getLastUpdateDate()));
				invWmAdjustmentsObj.setUnidadMedida(data.getUnidadMedida());
				invWmAdjustmentsObj.setFechaDespacho(null);
				invWmAdjustmentsObj.setFechaProgramada(null);
				invWmAdjustmentsObj.setFechaRecepcion(null);
				invWmAdjustmentsObj.setGuiaDespacho(0);
				invWmAdjustmentsObj.setInterfaceHeaderId(0);
				invWmAdjustmentsObj.setInterfaceLineId(0);
				invWmAdjustmentsObj.setShipmentNumber(0);
				invWmAdjustmentsObj.setShipmentHeaderId(0);
				invWmAdjustmentsObj.setShipmentLineId(0);
				invWmAdjustmentsObj.setPoHeaderId(0);
				invWmAdjustmentsObj.setPoLineLocationId(0);
				invWmAdjustmentsObj.setDestinationTypeCode("");
				invWmAdjustmentsObj.setVendorId(0);
				invWmAdjustmentsObj.setVendorSiteId(0);
				invWmAdjustmentsObj.setPoLineId(0);
				invWmAdjustmentsObj.setShipmentLine(0);
				invWmAdjustmentsObj.setLocationId(0);
				invWmAdjustmentsObj.setPersonId(0);
				invWmAdjustmentsObj.setCanalVenta(data.getCanalVenta());
				invWmAdjustmentsObj.setUnidadOperativa(data.getUnidadOperativa());
				invWmAdjustmentsObj.setTransactionTypeWms(data.getTransactionType());
				invWmAdjustmentsObj.setCantSprov(0);
				invWmAdjustmentsObj.setCantCprov(0);
				invWmAdjustmentsObj.setAccrualCcid(0);
				invWmAdjustmentsObj.setVarianceCcid(0);
				invWmAdjustmentsObj.setBudgetCcid(0);
				invWmAdjustmentsObj.setTipoCierreOc((char) 0);
				invWmAdjustmentsObj.setEstadoRecepcionOn(0);
				invWmAdjustmentsObj.setAtributte1(data.getAtributte1());
				invWmAdjustmentsObj.setAtributte2(data.getAtributte2());
				invWmAdjustmentsObj.setAtributte3(data.getAtributte3());
				invWmAdjustmentsObj.setAtributte4(data.getAtributte4());
				invWmAdjustmentsObj.setAtributte5(data.getAtributte5());
				
				invWmAdjustmentsList.add(invWmAdjustmentsObj);
			}
			
			this.invWmAdjustments.saveAll(invWmAdjustmentsList);
			this.kafkaTemplate.send(IConstants.OPERATION_SUCCESS_TOPIC, "Operation Success!");
		} catch (ParseException e) {
			System.err.println(e);
			this.kafkaTemplate.send(IConstants.OPERATION_ERROR_TOPIC, "Operation Error -> " + e.getMessage());
		} catch (Exception e) {
			System.err.println(e);
			this.kafkaTemplate.send(IConstants.OPERATION_ERROR_TOPIC, "Operation Error -> " + e.getMessage());
		}
	}
	
	/**
	 * Simple Date Format to Message Date
	 * 
	 * @param pDate
	 * @return Message Date
	 */
	private String getMessageDate(long pDate) {
		Date date = new Date(pDate);
		SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yy");
        return df2.format(date);
	}
	
	/**
	 * Convert String to SQL Date
	 * @param pDate
	 * @return SQL Date
	 * @throws ParseException
	 */
	private java.sql.Date stringToSqlDate(String pDate) throws ParseException{
		Date date = this.stringToDate(pDate);
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		return sqlDate;
	}
	
	/**
	 * Convert String to Date Format yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
	 * @param pDate
	 * @return Date
	 * @throws ParseException
	 */
	private Date stringToDate(String pDate) throws ParseException {
		Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(pDate);
		return date;
	}
}
