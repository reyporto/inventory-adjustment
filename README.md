# Development environment

## Git Clone Repo

```shell
> git clone https://reinaldoporto@bitbucket.org/reyporto/inventory-adjustment.git
```

## Initial Enviroment

- **Containers oracle-xe-11g, apache-zookeeper, apache-kafka**
- **Create Kafka Topics.**

```shell
> cd {CLONE_REPO_FOLDER}/inventory-adjustment/exec-project
> chmod 775 init.sh
> ./init.sh
> Options: 1- Install 2- Start Producer 3- Start Consumer: 1
```

##### Output Result

```shell
> Step 1: Creating containers oracle-xe-11g, apache-zookeeper, apache-kafka.
> Creating apache-zookeeper ... done
> Creating oracle-xe-11g    ... done
> Creating apache-kafka     ... done
> Step 2: Kafka Topics.
> Waiting for 10 seconds...
> Creating KAF40021-1-0-SupplyChainErpService-InventoryAdjusment Topic...
> Created topic KAF40021-1-0-SupplyChainErpService-InventoryAdjusment.
> Creating KAF40022-1-0-SupplyChainErpService-OperationError Topic...
> Created topic KAF40022-1-0-SupplyChainErpService-OperationError.
> Creating KAF40023-1-0-SupplyChainErpService-OperationSuccess Topic...
> Created topic KAF40023-1-0-SupplyChainErpService-OperationSuccess.
```

## Kafka Producer

- **KAF40021-1-0-SupplyChainErpService-InventoryAdjusment**

##### Step 1: Open new terminal tab.
##### Step 2: Go to exec-project folder. 

```shell
> cd {CLONE_REPO_FOLDER}/inventory-adjustment/exec-project
```

##### Step 3: Kafka Console Producer

```shell
> ./init.sh
> Options: 1- Install 2- Start Producer 3- Start Consumer: 2
```

##### Output Result

```shell
> KAF40021-1-0-SupplyChainErpService-InventoryAdjusment Producer...
> >
```

***Note: Here you can send messages to the consumer.***

## Kafka Consumers

- **KAF40022-1-0-SupplyChainErpService-OperationError**

##### Step 1: Open new terminal tab.
##### Step 2: Go to exec-project folder. 

```shell
> cd {CLONE_REPO_FOLDER}/inventory-adjustment/exec-project
```

##### Step 3: Kafka Console Producer

```shell
> ./init.sh
> Options: 1- Install 2- Start Producer 3- Start Consumer: 3
> Consumer: 1- OperationError 2- OperationSuccess: 1
```

##### Output Result

```shell
> KAF40022-1-0-SupplyChainErpService-OperationError Consumer...
>
```

- **KAF40023-1-0-SupplyChainErpService-OperationSuccess**

##### Step 1: Open new terminal tab.
##### Step 2: Go to exec-project folder. 

```shell
> cd {CLONE_REPO_FOLDER}/inventory-adjustment/exec-project
```

##### Step 3: Kafka Console Producer

```shell
> ./init.sh
> Options: 1- Install 2- Start Producer 3- Start Consumer: 3
> Consumer: 1- OperationError 2- OperationSuccess: 2
```

##### Output Result

```shell
> KAF40022-1-0-SupplyChainErpService-OperationSuccess Consumer...
>
```

## Start Spring Boot App

- **Import Maven Project in your IDE.**
- **Run Spring Boot App -> Kaf4021010Application.**
- **Send Message producer KAF40021-1-0-SupplyChainErpService-InventoryAdjusment**

```json
{"id":"43d84da9-a31f-47c7-89d4-20ed615a090f","user":"roymer","nombreOperacion":"inventoryAdjustment","data":[{"unidadOperativa":"WOMMA","codOrganizacionDesde":"AAA","codOrganizacionHasta":"ZZZ","transactionType":"PAGO ONLINE","codigoSku":"2","unidadMedida":"UNI","cantidad":12.0,"canalVenta":"CANALVENTA_WEB","serie":"A0004300001002302321","subinventoryCodeDesde":"desa","subinventoryCodeHasta":"ZZZZ","transactionDate":"2019-08-02T23:16:53.646Z","atributte1":"test1","atributte2":"test2","atributte3":"test3","atributte4":"test","atributte5":"test","createdBy":0,"createdDate":"2019-08-02T23:16:53.646Z","lastUpdateDate":"2019-08-02T23:16:53.646Z","lastUpdateBy":0}],"fecha_mensaje":1575643453380}
```

- **http://localhost:8090/ To validate Inventory Adjustment.**
